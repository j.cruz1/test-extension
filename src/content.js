chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
  if (message.action === "getRequests") {
    requests = performance.getEntriesByType("resource");
    chrome.runtime.sendMessage(message = { action: "collectRequests", requests });
  }
});
