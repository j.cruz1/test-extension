chrome.runtime.onInstalled.addListener(async () => {
  console.log("Extension installed");
});

chrome.webNavigation.onCompleted.addListener((details) => {
  chrome.tabs.sendMessage(details.tabId, { action: "getRequests" });
});

const requestUrls = new Set();
chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message.action === "collectRequests") {
    for (const request of message.requests) {
      requestUrls.add(request.name);
    }
    console.log(`Tab requests: ${message.requests.length}`)
    console.log(`Total requets: ${requestUrls.size}`)
  }
});
