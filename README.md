# test-extension

This is a test extension. Doesn't have specific functionality but aims as a 
dummy extension for multiple purposes.

## Getting started

```bash
npm ci
npx webpack
```
